import { GraphQLID, GraphQLObjectType, GraphQLString, GraphQLNonNull } from "graphql";

const User = new GraphQLObjectType({
    name: 'User',
    description: 'User object as defined in SQL DB',
    fields: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        username: { type: GraphQLString },
        name: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: ({ firstName, lastName }) => 
                [firstName, lastName].filter(Boolean).join(' ')
        }
    }
});

export default User;