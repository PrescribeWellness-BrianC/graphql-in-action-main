import { GraphQLObjectType, GraphQLInt, GraphQLNonNull, GraphQLFloat } from "graphql";

const NumbersInRange = new GraphQLObjectType({
    name: 'NumbersInRange',
    description: 'Aggregate info on a range of numbers',
    fields: {
        begin: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        end: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        sum: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        count: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        avg: {
            type: new GraphQLNonNull(GraphQLFloat)
        }
    },
    
});

export default NumbersInRange;
