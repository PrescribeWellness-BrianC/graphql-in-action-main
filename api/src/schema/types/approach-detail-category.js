import { GraphQLEnumType } from 'graphql';

const ApproachDetailCateory = new GraphQLEnumType({
    name: 'ApproachDetailCategory',
    values: {
        NOTE: {},
        EXPLANATION: {},
        WARNING: {}
    }
})

export default ApproachDetailCateory;