import { GraphQLID, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLNonNull, GraphQLList } from "graphql";

import User from './user';
import ApproachDetail from './approach-detail';

const Approach = new GraphQLObjectType({
    name: 'Approach',
    fields: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        content: { type: new GraphQLNonNull(GraphQLString) },
        voteCount: { type: new GraphQLNonNull(GraphQLString) },
        createdAt: { type: new GraphQLNonNull(GraphQLString),
            resolve: (source) => source.createdAt.toISOString() },
        author: { type: new GraphQLNonNull(User),
            resolve: (source, args, { loaders }) =>
                loaders.users.load(source.userId) },
        detailList: { type: new GraphQLNonNull( new GraphQLList( new GraphQLNonNull( ApproachDetail ))),
            resolve: (source, args, { loaders }) =>
                loaders.detailLists.load(source.id) }
    }
});

export default Approach;
