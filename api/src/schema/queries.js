import { GraphQLID, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLNonNull, GraphQLList } from "graphql";
import NumbersInRange from "./types/numbersInRange";
import { numbersInRange } from "../utils";
import  Task from './types/task';


const QueryType = new GraphQLObjectType({
    name: 'Query',
    fields: {
        currentTime: {
            type: GraphQLString,
            resolve: () => {
             const isoString = new Date().toISOString();
             return isoString.slice(11, 19);
            },
        },
        numbersInRange: {
            type: NumbersInRange,
            args: {
                begin: {type: new GraphQLNonNull(GraphQLInt)},
                end: {type: new GraphQLNonNull(GraphQLInt)}
            },
            resolve: function(source, {begin, end}) {
                return numbersInRange(begin, end);
            }
        },
        taskMainList: {
            type: new GraphQLList(new GraphQLNonNull(Task)),
            resolve: async (source, args, { pgApi }) => {
                return pgApi.taskMainList();
            }
        },
        taskInfo: {
            type: Task,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve: async (source, args, { loaders }) => {
                return loaders.tasks.load(args.id);
            }
        }
    },
});

export default QueryType;
