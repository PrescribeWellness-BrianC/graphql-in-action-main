import crypto from 'crypto';

export const randomString = (bytesSize = 32) =>
  crypto.randomBytes(bytesSize).toString('hex');

export const numbersInRange = (begin, end) => {
  if (end <= begin) {
    throw Error(`Invalid range: ${begin} - ${end}`);
  }

  let sum = 0;
  let count = 0;
  let avg = 0;

  for (let i = begin; i <= end; ++i) {
    sum += i;
    ++count;
  }

  avg = sum / count;

  return {begin, end, sum, count, avg};
};

export const extractPrefixedColumns = ({
  prefixedObject,
  prefix
}) => {
  const prefixRexp = new RegExp(`^${prefix}_(.*)`);
  return Object.entries(prefixedObject).reduce(
    (acc, [key, value]) => {
      const match = key.match(prefixRexp);
      if (match) {
        acc[match[1]] = value
      }

      return acc;
    },
    {},
  );
};
