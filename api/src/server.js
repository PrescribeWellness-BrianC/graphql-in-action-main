//import { graphql } from 'graphql';
import { graphqlHTTP } from 'express-graphql';
//import { schema, rootValue } from './schema';
import { schema } from './schema';
import pgClient from './db/pg-client';
import pgApiWrapper from './db/pg-api';
import DataLoader from 'dataloader';
import mongoApiWrapper from './db/mongo-api';

// NOTE: To run this code on the node command line:
//  node -r esm api/src/server.js "{ currentTime }"
//
// const mySchema = schema;
// const myRootValue = rootValue;

// const executeGraphQLRequest = async request => {
//   console.log('schema: ' + mySchema);
//   const resp = await graphql({ "schema": mySchema, "source": request, "rootValue": myRootValue });
//   console.log(resp.data);
// };

// console.log('argv[2]: ' + process.argv[2]);
// executeGraphQLRequest(process.argv[2]);

/** GIA NOTES
 *
 * Use the code below to start a bare-bone express web server
*/

import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import https from 'https';
import fs from 'fs';

import * as config from './config';

async function main() {
  //const { pgPool } = await pgClient();
  const pgApi = await pgApiWrapper();
  const mongoApi = await mongoApiWrapper();
  const server = express();
  server.use(cors());
  server.use(morgan('dev'));
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(bodyParser.json());
  server.use('/:fav.ico', (req, res) => res.sendStatus(204));

  // Example route
  // server.use('/', (req, res) => {
  //   res.send('Hello World');
  // });

  //currentTime route...
  const mySchema = schema;
  //const myRootValue = rootValue;
  server.use('/', (req, res) => {
    const loaders = {
      users: new DataLoader((userIds) => pgApi.usersInfo(userIds)),
      approachLists: new DataLoader((taskIds) => pgApi.approachLists(taskIds)),
      tasks: new DataLoader((taskIds) => pgApi.tasksInfo(taskIds)),
      detailLists: new DataLoader((approachIds) => mongoApi.detailLists(approachIds))
    };

    graphqlHTTP({
      "schema": mySchema, 
      "graphiql": true,
      "context": { pgApi, loaders }
    }) (req, res);
  }); 

  // // This line runs the server
  // server.listen(config.port, () => {
  //   console.log(`Server URL: http://localhost:${config.port}/`);
  // });

  //Load self-signed certificate files...
  //To create the files, see: https://medium.com/@nitinpatel_20236/how-to-create-an-https-server-on-localhost-using-express-366435d61f28
  //
  // Node.js window commands:
  // $ openssl req -x509 -newkey rsa:2048 -keyout keytmp.pem -out cert.pem -days 365
  // $ openssl rsa -in keytmp.pem -out key.pem
  //
  const key = fs.readFileSync('../certificates/self-signed/key.pem');
  const cert = fs.readFileSync('../certificates/self-signed/cert.pem');

  //create the HTTPS server 
  const serverHTTPS = https.createServer({key: key, cert: cert}, server);

  //serverHTTPS.listen(3001, () => { console.log('listening on 3001') });
  serverHTTPS.listen(config.port, () => { console.log(`listening on: ${config.port}`) });

}

main();


