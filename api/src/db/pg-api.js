import pgClient from './pg-client';
import sqls from './sqls';

const pgApiWrapper = async () => {
    const { pgPool } = await pgClient();
    const pgQuery = async (text, params = {} ) => {
        return ( await pgPool.query(text, Object.values(params)) );
    };

    return {
        taskMainList: async () => {
            console.log('sql.tasksLatest: ' + sqls.tasksLatest);
            const pgResp = await pgQuery(sqls.tasksLatest);
            return pgResp.rows;
        },
        usersInfo: async (userIds) => {
            const pgResp = await pgQuery(sqls.usersFromIds, {$1: userIds});

            return userIds.map((userId) => 
                pgResp.rows.find((row) => userId === row.id));
        },
        approachLists: async (taskIds) => {
            const pgResp = await pgQuery(sqls.approachesForTaskIds, {$1: taskIds  });
            
            return taskIds.map((taskId) => 
                pgResp.rows.filter((row) => taskId === row.taskId) 
                );
        },
        tasksInfo: async (taskIds) => {
            const pgResp = await pgQuery(sqls.tasksFromIds, {
                $1: taskIds,
                $2: null
            });

            return taskIds.map((taskId) => 
                pgResp.rows.find((row) => taskId == row.id)
            );
        }
    };
};

export default pgApiWrapper;
